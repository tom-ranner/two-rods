from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import sys

if __name__ == '__main__':
    fig = plt.figure(1)
    ax = fig.add_subplot(111, projection='3d')
    m, M = (0.0, 0.0)

    for fn in sys.argv[1:]:
        data = np.load(fn)
        coms = data['arr_0']

        if coms.shape[1] < 3:
            z = np.zeros((coms.shape[0], 3-coms.shape[1]))
            coms = np.concatenate((coms, z), axis=1)

        mid = np.mean(coms, axis=0)
        m = min(np.min(coms-mid), -0.5, m)
        M = max(np.max(coms-mid), 0.5, M)

        ax.plot(coms[:, 0], coms[:, 1], coms[:, 2])
        ax.set_xlim(mid[0]+m, mid[0]+M)
        ax.set_ylim(mid[1]+m, mid[1]+M)
        ax.set_zlim(mid[1]+m, mid[1]+M)

    plt.show()
