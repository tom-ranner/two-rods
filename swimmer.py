import numpy as np
from scipy.optimize import minimize
from scipy.optimize.nonlin import NoConvergence
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import sys

K = 2
dt = 1.0e-2
length = 0.5


class Geometry_twodimensional:
    def __init__(self, **kwargs: dict):
        self._x0 = np.array([0, 0])
        self._alpha = 0.0
        self._theta = None

        if 'x0' in kwargs.keys():
            self._x0 = kwargs['x0']
        if 'alpha' in kwargs.keys():
            self._alpha = kwargs['alpha']
        if 'theta' in kwargs.keys():
            self._theta = kwargs['theta']
        if 'X' in kwargs.keys():
            X = kwargs['X']
            self._x0 = np.array([X[0], X[1]])
            self._alpha = X[2]

        assert(self._theta is not None)

    def d1(self):
        alpha = self._alpha
        return np.array([np.cos(alpha), np.sin(alpha)])

    def d2(self):
        alpha = self._alpha
        theta = self._theta
        return np.array([np.cos(alpha+theta), np.sin(alpha+theta)])

    def x0(self):
        return self._x0

    def x1(self):
        return self._x0 + self.d1()

    def x2(self):
        return self._x0 + length*self.d2()

    def toX(self):
        return np.array([self._x0[0], self._x0[1], self._alpha])

    def com(self):
        return self.x0() + self.x1()/2.0 + length*self.x2()/2.0


class Geometry_twoplanes:
    def __init__(self, **kwargs: dict):
        self._x0 = np.array([0, 0, 0])
        self._alpha = np.pi/2.0
        self._theta = None

        if 'x0' in kwargs.keys():
            self._x0 = kwargs['x0']
        if 'alpha' in kwargs.keys():
            self._alpha = kwargs['alpha']
        if 'theta' in kwargs.keys():
            self._theta = kwargs['theta']
        if 'X' in kwargs.keys():
            X = kwargs['X']
            self._x0 = np.array([X[0], X[1], X[2]])
            self._alpha = X[3]

        assert(self._theta is not None)

    def d1(self):
        alpha = self._alpha
        return np.array([0, np.cos(alpha), np.sin(alpha)])

    def d2(self):
        alpha = self._alpha
        theta = self._theta
        b = np.cos(theta)/np.sin(alpha)
        # assert(1-b*b >= 0)
        a = np.sqrt(1-b*b)
        return np.array([a, 0, b])

    def x0(self):
        return self._x0

    def x1(self):
        return self._x0 + self.d1()

    def x2(self):
        return self._x0 + length*self.d2()

    def toX(self):
        return np.array([self._x0[0], self._x0[1], self._x0[2], self._alpha])

    def com(self):
        return self.x0() + self.x1()/2.0 + length*self.x2()/2.0


class Geometry_d2free:
    def __init__(self, **kwargs: dict):
        self._x0 = np.array([0, 0, 0])
        self._alpha = 0.0
        self._phi = np.pi/2.0
        self._theta = None

        if 'x0' in kwargs.keys():
            self._x0 = kwargs['x0']
        if 'alpha' in kwargs.keys():
            self._alpha = kwargs['alpha']
        if 'phi' in kwargs.keys():
            self._phi = kwargs['phi']
        if 'theta' in kwargs.keys():
            self._theta = kwargs['theta']
        if 'X' in kwargs.keys():
            X = kwargs['X']
            self._x0 = np.array([X[0], X[1], X[2]])
            self._alpha = X[3]
            self._phi = X[4]

        assert(self._theta is not None)

    def d1(self):
        alpha = self._alpha
        return np.array([0, np.cos(alpha), np.sin(alpha)])

    def d2(self):
        alpha = self._alpha
        phi = self._phi
        theta = self._theta

        d2_tilde = np.array([0, np.cos(alpha+theta), np.sin(alpha+theta)])
        ret = np.cos(phi) * d2_tilde \
            + np.sin(phi) * np.cross(self.d1(), d2_tilde) \
            + (1-np.cos(phi)) * np.dot(self.d1(), d2_tilde) * self.d1()
        return ret

    def x0(self):
        return self._x0

    def x1(self):
        return self._x0 + self.d1()

    def x2(self):
        return self._x0 + length*self.d2()

    def toX(self):
        return np.array([self._x0[0], self._x0[1], self._x0[2],
                         self._alpha, self._phi])

    def com(self):
        return self.x0() + self.x1()/2.0 + length*self.x2()/2.0


class Geometry_d2rottheta:
    def __init__(self, **kwargs: dict):
        self._x0 = np.array([0, 0, 0])
        self._alpha = 0.0
        self._theta = None

        if 'x0' in kwargs.keys():
            self._x0 = kwargs['x0']
        if 'alpha' in kwargs.keys():
            self._alpha = kwargs['alpha']
        if 'phi' in kwargs.keys():
            self._phi = kwargs['phi']
        if 'theta' in kwargs.keys():
            self._theta = kwargs['theta']
        if 'X' in kwargs.keys():
            X = kwargs['X']
            self._x0 = np.array([X[0], X[1], X[2]])
            self._alpha = X[3]

        assert(self._theta is not None)

    def d1(self):
        alpha = self._alpha
        return np.array([0, np.cos(alpha), np.sin(alpha)])

    def d2(self):
        alpha = self._alpha
        phi = self._theta
        theta = self._theta

        d2_tilde = np.array([0, np.cos(alpha+theta), np.sin(alpha+theta)])
        ret = np.cos(phi) * d2_tilde \
            + np.sin(phi) * np.cross(self.d1(), d2_tilde) \
            + (1-np.cos(phi)) * np.dot(self.d1(), d2_tilde) * self.d1()
        return ret

    def x0(self):
        return self._x0

    def x1(self):
        return self._x0 + self.d1()

    def x2(self):
        return self._x0 + length*self.d2()

    def toX(self):
        return np.array([self._x0[0], self._x0[1], self._x0[2], self._alpha])

    def com(self):
        return self.x0() + self.x1()/2.0 + length*self.x2()/2.0


def advance(geo_old, theta):
    # extract old values
    xn0 = geo_old.x0()
    xn1 = geo_old.x1()
    xn2 = geo_old.x2()

    # define geometry type used
    My_geometry = type(geo_old)

    # objective
    def obj(X):
        geo = My_geometry(X=X, theta=theta)
        x0 = geo.x0()
        x1 = geo.x1()
        x2 = geo.x2()
        d1 = geo.d1()
        d2 = geo.d2()

        def _K1(v):
            return np.dot(v, d1) * d1 + K * (v - np.dot(v, d1) * d1)

        def _K2(v):
            return np.dot(v, d2) * d2 + K * (v - np.dot(v, d2) * d2)

        def _K0(v):
            return 0.5*(_K1(v) + _K2(v))

        return np.dot(_K0(x0-xn0), x0-xn0) + np.dot(_K1(x1-xn1), x1-xn1) \
            + np.dot(_K2(x2-xn2), x2-xn2)

    # optimise and extract results
    ret = minimize(obj, geo_old.toX())
    geo = My_geometry(X=ret.x, theta=theta)

    return geo


def plot(geo, t):
    x0 = geo.x0()
    x1 = geo.x1()
    x2 = geo.x2()

    if x0.size == 3:
        xx = [v[0] for v in (x1, x0, x2)]
        yy = [v[1] for v in (x1, x0, x2)]
        zz = [v[2] for v in (x1, x0, x2)]
        ax.plot(xx, yy, zz, '.-')
    else:
        xx = [v[0] for v in (x1, x0, x2)]
        yy = [v[1] for v in (x1, x0, x2)]
        plt.plot(xx, yy, '.-')


if __name__ == '__main__':
    t = 0

    def theta_increasing(t): return np.pi*(1+t)
    def theta_fullrange(t):  return np.pi * (1+np.sin(t))
    def theta_halfrange(t):  return np.pi * (1+np.sin(t)**2)

    theta_case_dict = {
        'increasing': theta_increasing,
        'fullrange':  theta_fullrange,
        'halfrange':  theta_halfrange,
    }

    try:
        theta_case = sys.argv[1]
        theta = theta_case_dict[theta_case]
    except (IndexError, KeyError):
        raise RuntimeError('missing or bad argv[1]: theta_case options '
                           +', '.join(theta_case_dict.keys()))

    geo_case_dict = {
        'twoplanes': Geometry_twoplanes(theta=theta(t)),
        'd2free': Geometry_d2free(theta=theta(t)),
        'd2rottheta': Geometry_d2rottheta(theta=theta(t)),
        'twodimensional': Geometry_twodimensional(theta=theta(t)),
    }

    try:
        geo_case = sys.argv[2]
        geo_old = geo_case_dict[geo_case]
    except (IndexError, KeyError):
        raise RuntimeError('missing or bad argv[2]: geo_case options '
                           +', '.join(geo_case_dict.keys()))

    fig = plt.figure(1)
    if geo_old.x0().size == 3:
        ax = fig.add_subplot(111, projection='3d')
    else:
        ax = fig.add_subplot(111)

    write_step = 1.0
    next_write = 0.5
    coms = []

    while t < 24.0:
        t += dt

        geo = advance(geo_old, theta(t))
        com = geo.com()
        coms.append(com)

        if t > next_write:
            plot(geo, t)
            next_write += write_step
            print(f'writing at t={t}')

        tn = t
        geo_old = geo

    if geo_old.x0().size == 3:
        ax.plot([com[0] for com in coms],
                 [com[1] for com in coms],
                 [com[2] for com in coms], 'k')

        coms = np.array(coms)
        mid = np.mean(coms, axis=0)
        m = min(np.min(coms-mid), -0.5)
        M = max(np.max(coms-mid), 0.5)

        ax.set_xlim3d(mid[0]+m, mid[0]+M)
        ax.set_ylim3d(mid[1]+m, mid[1]+M)
        ax.set_zlim3d(mid[2]+m, mid[2]+M)
    else:
        ax.plot([com[0] for com in coms],
                [com[1] for com in coms], 'k')

        coms = np.array(coms)
        mid = np.mean(coms, axis=0)
        m = min(np.min(coms-mid), -0.5)
        M = max(np.max(coms-mid), 0.5)

        ax.set_xlim(mid[0]+m, mid[0]+M)
        ax.set_ylim(mid[1]+m, mid[1]+M)

    plt.savefig(f'res-{geo_case}-{theta_case}.svg')
    np.savez(f'{geo_case}-{theta_case}coms.npz', coms)
