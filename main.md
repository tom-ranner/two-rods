---
title: A two-rod swimmer
author: Tom Ranner
draft: true
link-citations: true
include-before: \
  \newcommand{\R}{\mathbb{R}}
  \renewcommand{\vec}[1]{\boldsymbol{#1}}
  \newcommand{\mat}[1]{\mathbb{#1}}
  \newcommand{\abs}[1]{|#1|}
---
We consider a two rod swimmer which has two rigid rods connected at a hinge immersed in a fluid which can be modelling as a linear drag fluid.

![](drawing.svg){ width=50% }

We consider the system as trying to satisfy three constraints - length of $e_1$, which we set to 1, length of $e_2$, which we set to $L$, and the angle between $e_1$ and $e_2$ which we set to $\theta$. 
We take a parameterisation in terms of $\vec{x}_0$ and an angle $\alpha$:
$$
 \vec{x}_1 = \vec{x}_0 + \vec{d}_1, \qquad
 \vec{x}_2 = \vec{x}_0 + L \vec{d}_2,
$$
where
$$
 \vec{d}_1 = (\cos(\alpha), \sin(\alpha)), \qquad
 \vec{d}_2 = (\cos(\alpha+\theta), \sin(\alpha+\theta)).
$$

The fluid is modelled using linear drag coefficients. We denote by $\mat{K}_j \dot{\vec{x}}_j$ the drag force at vertex $j$ where $\mat{K}_j$ is given by
$$
 \mat{K}_j = ( \vec{d}_j \otimes \vec{d}_j ) + K ( \mat{I} - \vec{d}_j \otimes \vec{d}_j ), \qquad \mbox{ for } j=1,2,
$$
and use $\mat{K}_0 = (\mat{K}_1 + \mat{K}_2)/2$.

We use a mnimising movements time stepping strategy. Given positions $(\vec{x}_0^n, \vec{x}_1^n, \vec{x}_2^n)$ at time $t_n$, we find $(\vec{x}_0, \alpha)$ by minimsing
$$
 F(\vec{x}_0, \alpha)
 = \sum_{j=0,1,2} \mat{K}_j (\vec{x}_j - \vec{x}_j^n) \cdot (\vec{x}_j - \vec{x}_j^n).
$$
Here we have applied the parameterisation of $\vec{x}_1$ and $\vec{x}_2$ in terms of $\vec{x}_0$ and $\alpha$. This parameterisation is used again in order to find the new values of $\vec{x}_1$ and $\vec{x}_2$.

The results look a bit like this:
![](res.svg){ width=80% }

The black curve shows the centre of mass over time. The coloured lines show different configurations at different times. We take $\theta = (1+t)\pi$, a time step $\Delta t = 10^{-2}$ and $K=2$.
