# Two rod model

Simulation of a two-rod swimmer in a viscous fluid. Details of the model and numerical scheme are in `main.md`. Dependencies are listed in `environment.yml`.
